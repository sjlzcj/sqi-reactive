package com.sqi.reactive

import com.sqi.reactive.common.repository.DynamicQueryRepositoryFactoryBean
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories

@SpringBootApplication
@EnableR2dbcRepositories(repositoryFactoryBeanClass = DynamicQueryRepositoryFactoryBean::class)
class SqiReactiveApplication

fun main(args: Array<String>) {
    runApplication<SqiReactiveApplication>(*args)
}
