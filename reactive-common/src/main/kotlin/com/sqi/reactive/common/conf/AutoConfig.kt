package com.sqi.reactive.common.conf

import com.sqi.reactive.common.auth.RequestContext
import com.sqi.reactive.common.repository.DynamicQueryRepositoryFactoryBean
import com.sqi.reactive.common.token.TokenHelper
import com.sqi.reactive.common.token.impl.JWTTokenHelper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories

/**
 * @author sjl
 * @date 2020/11/13
 */

@Configuration
class AutoConfig {
    @Bean
    fun requestContext(): RequestContext = RequestContext()
    
    @Bean
    fun tokenHelp(): TokenHelper = JWTTokenHelper()
}