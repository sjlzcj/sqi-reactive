package com.sqi.reactive.common.annoation

/**
 * 标记当前接口不走登录拦截器
 * @author sjl
 * @date 2020/11/12
 */
@kotlin.annotation.Target(AnnotationTarget.FUNCTION)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class IgnoreAuth