package com.sqi.reactive.common.auth

/**
 * @author: zhangfx
 * @create: 2019-02-27 13:53
 */
data class AuthPayload(
        var type: String? = null,
        var subject: Any? = null,
        var projectCode: String? = null
) {
    companion object {
        fun of(type: String?, subject: Any?): AuthPayload {
            val payload = AuthPayload()
            payload.type = type
            payload.subject = subject
            return payload
        }
    }
}