package com.sqi.reactive.common.util

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * @author sjl
 * @date 2020/6/8
 */

val linePattern: Pattern = Pattern.compile("_(\\w)")

/**
 * 下划线转驼峰
 */
fun String.lineToHump(): String {
    val matcher: Matcher = linePattern.matcher(this)
    val sb = StringBuffer()
    while (matcher.find()) {
        matcher.appendReplacement(sb, matcher.group(1).toUpperCase())
    }
    matcher.appendTail(sb)
    return sb.toString()
}

/**
 * 驼峰转下划线
 */
fun String.humpToLine(): String {
    return this.replace("[A-Z]".toRegex(), "_$0").toLowerCase()
}

/**
 * 下划线前缀
 */
fun String.getPrefix(str: String): String = getPrefixWithSeparator("_") ?: str

/**
 * 下划线后缀
 */
fun String.getPostfix(): String = getPostfixWithSeparator("_") ?: this

/**
 * 获取文件名不包含扩展名
 */
fun String.getFileNameWithoutExtension() = getPrefixWithSeparator(".") ?: this

/**
 * 获取扩展名
 */
fun String.getFileExtension(): String? = getPostfixWithSeparator(".")

/**
 * 根据给定分隔符获取前缀，不支持分隔符
 * @param separator 分隔符，若不存在返回null
 */
fun String.getPrefixWithSeparator(separator: String): String? {
    val index = this.indexOf(separator)
    return if (index >= 0) {
        this.substring(0, index)
    } else {
        null
    }
}

/**
 * 根据给定分隔符获取后缀，不支持分隔符
 * @param separator 分隔符，若不存在返回null
 */
fun String.getPostfixWithSeparator(separator: String): String? {
    val index = this.indexOf(separator)
    return if (index >= 0) {
        this.substring(index + 1)
    } else {
        null
    }
}