@file:Suppress("UNUSED_PARAMETER", "UNUSED")

package com.sqi.reactive.common.util

import com.google.common.collect.Maps

/**
 * 创建动态查询Map </br>
 * @sample paramsBuilderSample
 *
 * @author sjl
 * @date 2020/6/17
 */
object ParamsBuilder {
    fun where(block: MutableMap<String, Any>.() -> Unit): MutableMap<String, Any> {
        val map: MutableMap<String, Any> = Maps.newHashMap()
        block(map)
        return map
    }
}

infix fun MutableMap<String, Any>.put(pair: Pair<String, Any>) {
    this[pair.first] = pair.second
}

/**
 * Operator
 */
infix fun String.eq(any: Any): Pair<String, Any> = "EQ_$this" to any

infix fun String.lt(any: Any): Pair<String, Any> = "LT_$this" to any

infix fun String.elt(any: Any): Pair<String, Any> = "ELT_$this" to any

infix fun String.gt(any: Any): Pair<String, Any> = "GT_$this" to any

infix fun String.egt(any: Any): Pair<String, Any> = "EGT_$this" to any

infix fun String.lk(any: Any): Pair<String, Any> = "LK_$this" to any

infix fun String.llk(any: Any): Pair<String, Any> = "LLK_$this" to any

infix fun String.rlk(any: Any): Pair<String, Any> = "RLK_$this" to any

infix fun String.orderBy(any: Any): Pair<String, Any> = "ORDER_$this" to any

infix fun String.`in`(any: Any): Pair<String, Any> = "IN_$this" to any

private fun paramsBuilderSample() {
    ParamsBuilder.where {
        put("fieldNameA" eq "value")
        put("fieldNameB" lt 0)
    }
}