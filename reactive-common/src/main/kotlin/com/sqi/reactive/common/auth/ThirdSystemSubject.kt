package com.sqi.reactive.common.auth


/**
 * @author: zhangfx
 * @create: 2019-02-27 13:47
 */
class ThirdSystemSubject (
    var id: Long? = null,
    var code: String? = null,
    var name: String? = null
)