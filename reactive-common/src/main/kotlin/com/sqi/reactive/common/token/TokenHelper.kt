package com.sqi.reactive.common.token

import com.sqi.reactive.common.auth.AuthPayload
import com.sqi.reactive.common.auth.MicroSubject
import com.sqi.reactive.common.auth.Subject
import com.sqi.reactive.common.auth.ThirdSystemSubject

/**
 * Token generator and verify
 * @author tarsean
 */
interface TokenHelper {
    /**
     * 根据用户信息生成token
     * @param subject
     * @return 生成的token字符串
     */
    fun generate(subject: Subject): String
    /**
     * 根据微服务信息生成token
     * @param subject
     * @return 生成的token字符串
     */
    fun generate(subject: MicroSubject): String?

    /**
     * 根据第三方系统信息生成token
     * @param subject
     * @return 生成的token字符串
     */
    fun generate(subject: ThirdSystemSubject): String?

    /**
     * 根据访问主体信息生成token
     * @param subject
     * @return 生成的token字符串
     */
    fun generate(subject: AuthPayload): String

    /**
     * 验证token，并还原访问主体信息
     * @param token
     * @return 解析后的对象，如果验证失败，返回null
     */
    fun verify(token: String?): AuthPayload?

    /**
     * 获取token id，token id唯一标识一个token，全局唯一
     * @return null 如果解析错误,否则返回对应的token id
     */
    fun tokenId(token: String): String
}