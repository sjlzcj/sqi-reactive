package com.sqi.reactive.common.result

/**
 * 分页结果集
 */
data class PageResult<T>(var total: Long = 0, var records: List<T>? = null)