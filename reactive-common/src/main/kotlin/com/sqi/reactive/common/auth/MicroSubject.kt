package com.sqi.reactive.common.auth

/**
 * @author: zhangfx
 * @create: 2019-02-27 13:46
 */
data class MicroSubject(
        var id: Long? = null,
        var code: String? = null,
        var name: String? = null,
        var basePath: String? = null
)