package com.sqi.reactive.common.auth.exception

/**
 * @author sjl
 * @date 2020/11/12
 */
class AuthException(override val message: String) : RuntimeException(message)