package com.sqi.reactive.common.repository

import com.sqi.reactive.common.result.PageResult
import org.springframework.data.domain.Pageable
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.relational.core.query.Query
import org.springframework.data.repository.NoRepositoryBean
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

/**
 * @author sjl
 * @date 2020/5/29
 */
@NoRepositoryBean
interface DynamicQueryRepository<T, ID> : R2dbcRepository<T, ID> {
    /**
     * 根据条件查询所有
     *
     * @param params 查询条件
     */
    fun findAll(params: Map<String, Any>?): Flux<T>

    /**
     * 根据条件查询所有,带分页
     * @param params 查询条件
     * @param pageable 分页条件
     */
    fun findAll(params: Map<String, Any>?, pageable: Pageable): Mono<PageResult<T>>

    /**
     * 查询所有
     *
     * @param query
     */
    fun findAll(query: Query): Flux<T>

    /**
     * 分页查询
     * @param query
     * @param pageable
     */
    fun findAll(query: Query, pageable: Pageable): Mono<PageResult<T>>

    /**
     * 返回元数据描述
     */
    fun metaInfo(): Mono<Map<String, Class<*>>>
}