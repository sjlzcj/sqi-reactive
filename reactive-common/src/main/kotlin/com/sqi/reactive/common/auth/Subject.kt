package com.sqi.reactive.common.auth

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * @author: zhangfx
 * @create: 2018-08-07 16:13
 */
@ApiModel(value = "Subject", description = "当前登录用户")
data class Subject(
        var id: String? = null,
        @ApiModelProperty(name = "username", value = "用户名")
        var username: String? = null,
        @ApiModelProperty(name = "realName", value = "用户真实名称")
        var realName: String? = null,
        var org: String? = null,
        @ApiModelProperty(name = "orgName", value = "用户部门名称")
        var orgName: String? = null,
        /**
         * 角色
         */
        @ApiModelProperty(name = "roles", value = "用户角色列表")
        var roles: List<String>? = null
)