@file:Suppress("UNUSED_PARAMETER", "UNUSED")

package com.sqi.reactive.common.util.query

import org.springframework.data.relational.core.query.Criteria
import org.springframework.data.relational.core.query.Query

/**
 * 动态创建Query
 * @sample queryBuilderSample
 *
 * @author sjl
 * @date 2020/6/17
 */
object QueryBuilder {
    fun where(block: () -> Criteria): Query = Query.query(block())
}

/**
 * Operator
 */
infix fun String.eq(any: Any): Criteria = Criteria.where(this).`is`(any)

infix fun String.lt(any: Any): Criteria = Criteria.where(this).lessThan(any)

infix fun String.elt(any: Any): Criteria = Criteria.where(this).lessThanOrEquals(any)

infix fun String.gt(any: Any): Criteria = Criteria.where(this).greaterThan(any)

infix fun String.egt(any: Any): Criteria = Criteria.where(this).greaterThanOrEquals(any)

infix fun String.lk(any: Any): Criteria = Criteria.where(this).like("%${any}%")

infix fun String.llk(any: Any): Criteria = Criteria.where(this).like("%${any}")

infix fun String.rlk(any: Any): Criteria = Criteria.where(this).like("${any}%")

infix fun String.`in`(any: Array<String>): Criteria = Criteria.where(this).`in`(any)

infix fun Criteria.and(criteria: Criteria): Criteria = this.and(criteria)

infix fun Criteria.or(criteria: Criteria): Criteria = this.or(criteria)

private fun queryBuilderSample() {
    QueryBuilder.where {
        "FieldNameA" eq "AAA" and
                ("FieldNameB" gt 0 or ("FieldNameC" llk "BBB")) or
                ("FieldNameD" lk "CCC")
    }
}