@file:Suppress("UNUSED_PARAMETER", "UNUSED")

package com.sqi.reactive.common.util

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

/**
 * 过去
 */
object Past

/**
 * 将来
 */
object Future

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.nanoseconds(future: Future): LocalDateTime = baseTime().plusNanos(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.nanoseconds(past: Past): LocalDateTime = baseTime().minusNanos(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.microseconds(future: Future): LocalDateTime = baseTime().plusNanos(1000L * toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.microseconds(past: Past): LocalDateTime = baseTime().minusNanos(1000L * toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.milliseconds(future: Future): LocalDateTime = baseTime().plusNanos(1000000L * toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.milliseconds(past: Past): LocalDateTime = baseTime().minusNanos(1000000L * toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.seconds(future: Future): LocalDateTime = baseTime().plusSeconds(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.seconds(past: Past): LocalDateTime = baseTime().minusSeconds(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.minutes(future: Future): LocalDateTime = baseTime().plusMinutes(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.minutes(past: Past): LocalDateTime = baseTime().minusMinutes(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.hours(future: Future): LocalDateTime = baseTime().plusHours(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.hours(past: Past): LocalDateTime = baseTime().minusHours(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.days(future: Future): LocalDate = baseDate().plusDays(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.days(past: Past): LocalDate = baseDate().minusDays(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.weeks(future: Future): LocalDate = baseDate().plusWeeks(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.weeks(past: Past): LocalDate = baseDate().minusWeeks(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.months(future: Future): LocalDate = baseDate().plusMonths(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.months(past: Past): LocalDate = baseDate().minusMonths(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.years(future: Future): LocalDate = baseDate().plusYears(toLong())

/**
 * @see notes
 * @sample dateUtilSample
 */
infix fun Int.years(past: Past): LocalDate = baseDate().minusYears(toLong())

private fun baseDate(): LocalDate = LocalDate.now()

private fun baseTime(): LocalDateTime = LocalDateTime.now()

fun LocalDate.toDate(): Date = Date.from(this.atStartOfDay(ZoneId.systemDefault()).toInstant())

private fun dateUtilSample() {
    5 days Past
    7 years Future
}

/**
 * 时间工具
 *
 * @sample dateUtilSample
 * @author sjl
 * @date 2020/6/21
 */
private fun notes() {}