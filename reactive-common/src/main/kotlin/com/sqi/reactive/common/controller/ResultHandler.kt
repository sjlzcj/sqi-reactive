package com.sqi.reactive.common.controller

/**
 * @author sjl
 * @date 2020/6/10
 */
data class ResultHandler<T>(var code: Int = ResultHandlerType.SUCCESS.code,
                         var message: String? = ResultHandlerType.SUCCESS.message,
                         var data: T? = null) {
    constructor(type: ResultHandlerType) : this(code = type.code, message = type.message)

    constructor(type: ResultHandlerType, data: T?) : this(code = type.code, message = type.message, data = data)

    enum class ResultHandlerType(val code: Int, val message: String) {
        SUCCESS(0, "SUCCESS"),
        SYSTEM_ERROR(-1, "系统错误"),
        PARAMETER_ERROR(-1, "参数校验失败"),
        LOGIN_ERROR(-1, "用户未登陆")
    }
}

