package com.sqi.reactive

import com.google.common.base.Stopwatch
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.r2dbc.core.DatabaseClient
import reactor.core.publisher.Flux
import java.time.Duration
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@SpringBootTest
class SqiReactiveApplicationTests {
    @Autowired
    lateinit var databaseClient: DatabaseClient

    @Test
    fun contextLoads() {
        val count = 2
        val countDownLatch = CountDownLatch(2)
        val stopwatch=Stopwatch.createStarted()

        Flux.range(1, count).delayElements(Duration.ofSeconds(1L))
                .flatMap {
                    databaseClient.execute("SELECT * FROM DEMO where id=:id")
                            .bind("id", it)
                            .fetch().all()
                }.subscribe {
                    println(it)
                    println("Step Passing ${stopwatch.elapsed(TimeUnit.MILLISECONDS)}")
                    countDownLatch.countDown()
                }
        countDownLatch.await()
        println("All Passing ${stopwatch.elapsed(TimeUnit.MILLISECONDS)}")

    }

}
