//package com.sqi.reactive.test.conf
//
//import com.sqi.reactive.common.filter.AuthFilter
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import org.springframework.core.Ordered
//import org.springframework.core.annotation.Order
//import org.springframework.web.server.WebFilter
//
///**
// * @author sjl
// * @date 2020/11/13
// */
//@Configuration
//class AutoFilterConf {
//    @Bean
//    @Order(value = Ordered.HIGHEST_PRECEDENCE)
//    fun webfluxAuthFilter(): WebFilter {
//        return AuthFilter()
//    }
//}