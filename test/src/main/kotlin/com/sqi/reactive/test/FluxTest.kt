package com.sqi.reactive.test

import reactor.core.publisher.Mono

/**
 * @author sjl
 * @date 2020/12/15
 */
class FluxTest {
}

fun main() {
    val m1 = Mono.just(1)
    val m2 = Mono.just(2)
    val m3 = Mono.just(3)

    val mm = Mono.from(m1).zipWhen { m2 }

}