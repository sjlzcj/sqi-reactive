export default {
  btn: {
    searchBtn: '查询',
    resetBtn: '重置',
    saveBtn: '保存',
    cancelBtn: '取消'
  },
  table: {
    create: '新增',
    columns: {
      operation: '操作',
      operation_detail: '详情',
      operation_edit: '修改',
      operation_delete: '删除'
    }
  }
}
