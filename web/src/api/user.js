import request from '@/utils/request'

export function login(data) {
  return request.post(`/auth/login`, data)
  // return request.post(`/auth/login`, `username=${data.username}&password=${data.password}`)
  // return request.post(`/auth/login?username=${data.username}&password=${data.password}`)
}
export function getInfo(token) {
  return request({
    url: '/auth/userinfo',
    method: 'get',
    params: {}
  })
}

export function logout() {
  return request({
    url: '/vue-element-admin/user/logout',
    method: 'post'
  })
}
