import request from '@/utils/request'

const basePath = '/user/impUser'

export default {
  /**
   * 分页查询
   */
  page: (params) => request.get(`${basePath}/query_page`, { params }).then(resp => resp.data),
  /**
   * 列表查询
   */
  list: (params) => request.get(`${basePath}/query_list`, { params }).then(resp => resp.data),
  /**
   * 根据ID查询
   */
  get: (id) => request.get(`${basePath}/get/${id}`).then(resp => resp.data),
  /**
   * 保存
   * @param data
   */
  insert: (data) => request.post(`${basePath}/save`, data).then(resp => resp.data),
  /**
   * 修改
   * @param data
   */
  update: (data) => request.post(`${basePath}/update/${data.id}`, data).then(resp => resp.data),
  /**
   * 删除
   * @param id
   */
  delete: (id) => request.post(`${basePath}/delete/${id}`, { id }).then(resp => resp.data)
}